package controllers;

import javafx.application.Platform;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Controller class for the menu bar added in the view. For the most part events get handled here that got catched in the main view.
 * This class should only handle "backend" events and should not import any frontend related plugins or packages. This class will only be
 * called by its view (Main view).
 *
 * @author Max Geldner
 * @version 1.3i5
 */
public class MenuHandler {

    /**
     * Handles the exit button click in the upper menu.
     * @throws Exception : when exit failed
     */
    public void doExit() throws Exception {
        try {
            Platform.exit();
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Platform exit failed!");
        }
    }

    /**
     * Handles the new file button click in the upper menu.
     * @return String empty to act as new file
     */
    public String doNewFile() {
        return "";
    }

    /**
     * Opens a file. Lets the user choose to open a file if skipDialogue isn't set true. Else the file found under
     * the given path will be opened. Can be used for Plugin access to control the influence of the plugin first.
     * @param skipDialogue : True if the user shouldn't be able to choose which file he wants to open, false if the user should
     * @param givenPath : Only gets used if skipDialogue is true. Determines the path of the file that should be opened automatically
     * @return String[] output of the handleOpenFile method. The full text of the file and its path.
     */
    public String[] doOpenFile(Boolean skipDialogue, String givenPath) {
        try {
            String path = givenPath;
            if(!skipDialogue) {
                path = this.doBeforeOpen();
            }
            return this.handleOpenFile(path);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Saves a file. Lets the user choose to save a file at a wanted location if skipDialogue isn't set true. Else the file will be saved
     * at the given path. Can be used for Plugin access to control the influence of the plugin first.
     * @param lines : The content of the file splitted in it's lines
     * @param skipDialogue : True if the user shouldn't be able to choose which file he wants to save, false if the user should
     * @param givenPath : Only gets used if skipDialogue is true. Determines the path of the file that should be saved automatically
     * @return String of the path where the file was saved
     */
    public String doSaveFile(String[] lines, Boolean skipDialogue, String givenPath) {
        try {
            String path = givenPath;
            if(!skipDialogue) {
                path = this.doBeforeSave();
            }
            this.handleSaveFile(lines, path);
            return path;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    /**
     * Handles the open file button click in the upper menu.
     * @param path : The path of the file that should be opened
     * @return String[] with the path and content of the wanted file
     */
    private String[] handleOpenFile(String path) {
        try {
            Charset charset = Charset.defaultCharset();
            Path file = Paths.get(path);
            List<String> lines = Files.readAllLines(file, charset);
            String fullText = "";
            for(String line : lines) {
                fullText += line;
            }
            String[] returnArray = {path, fullText};
            return returnArray;
        } catch (IOException e) {
            e.getMessage();
            return null;
        }
    }

    /**
     * Handles the save file button click in the upper menu.
     * @param lines : All lines of the file contained in a String array
     * @param path : The path of the file that should be saved
     * @throws IOException gets thrown when there was an error in the save process. Gets handled in the view
     */
    private void handleSaveFile(String[] lines, String path) throws IOException {
        try {
            List<String> writtenLines = new ArrayList<String>();
            for (String line : lines) {
                writtenLines.add(line);
            }

            Path file = Paths.get(path);
            Files.write(file, writtenLines, Charset.defaultCharset());
        } catch(IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Executes the JavaFX fileChooser for saving files.
     * @return String with the path of the saved file
     * @throws Exception if the fileChooser didn't succeed to give back a location
     */
    private String doBeforeSave() throws Exception {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Secure Text files (*.secure)", "*.secure");
        fileChooser.getExtensionFilters().add(filter);
        File file = fileChooser.showSaveDialog(new Stage());

        if(file != null){
            String openFile = file.getAbsolutePath();
            return openFile;
        } else {
            throw new Exception("There was an Error while saving the file!");
        }
    }

    /**
     * Executes the JavaFX fileChooser for opening files.
     * @return String with the path of the opened file
     * @throws Exception if the fileChooser didn't succeed to give back a location
     */
    private String doBeforeOpen() throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Secure Text files (*.secure)", "*.secure");
        fileChooser.getExtensionFilters().add(filter);
        File file = fileChooser.showOpenDialog(new Stage());

        if(file != null){
            String openFile = file.getAbsolutePath();
            return openFile;
        } else {
            throw new Exception("The file you requested doesn't exist or is no longer valid!");
        }
    }

    /**
     * Checks if teh given plugin name is in the at the moment loaded plugins.
     * @param pluginList the full list of all loaded and active plugins
     * @param pluginName the name to check
     * @return true if the name is valid, false if not
     */
    public boolean isPNameValid(LinkedHashMap<String, Object> pluginList, String pluginName) {
        if(pluginList.containsKey(pluginName)) {
            return true;
        } else return false;
    }

} // End of class MenuHandler
