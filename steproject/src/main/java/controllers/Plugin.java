package controllers;

import views.Main;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javafx.scene.control.Menu;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;

/**
 * Class is the parent class for all plugins in the plugins folder.
 * Plugins can be imported from outside and are marked as used when specified in the plugin.XML-File.
 * Every plugin needs to have a start method with a Stage parameter. This enables the plugin to
 * modify the stage of the application. The loadPlugin method gets invoked automatically when loading the plugin.
 *
 * @author : Max Geldner
 * @version : 1.0i1
 */
public abstract class Plugin {

    // LinkedHashMap containing the plugin name as key and a LinkedHashMap with a key value pair of property name and property value as value
    private static LinkedHashMap<String, LinkedHashMap<String, String>> pluginProperties = new LinkedHashMap<>();

    // abstract methods must be implemented in every plugin for the STE
    /**
     * This method gets executed when a plugin gets loaded. Plugins should use this method for initialization. Also the view
     * gets handed to the plugin. The Plugin is able to modify the view with the methods that are public. Plugins can use this
     * if they want variables or properties of the view, for example the user written text.
     * @param view : The view of the main STE class
     */
    public abstract void loadPlugin(Main view);

    /**
     * This method allows the plugin to add submenus to its menu. If specified the a menu will be created for the plugin in the
     * main menu bar. Return a javafx classic menu object to add submenus to the plugins main menu in the menu bar. The plugin will be
     * handed it's empty main menu point that can be modified and filled.
     * @param menu : The empty plugin main Menu object that can be modified and filled with submenus
     * @return Menu javafx object, the filled menu of the plugin that will be inserted by the STEs main view class
     */
    public abstract Menu addSubMenus(Menu menu);

    /**
     * Loads all plugins specified and activated in the plugin.xml file. Returns a list with all active plugins.
     * @param view : The Main view of the text editor. The view is used as a reflection call parameter for the plugin class
     * @throws Exception when the loading of the plugin failed
     * @return activePlugins in a linked hash map. The name is the key and the main class of the plugin the value.
     */
    public static LinkedHashMap<String, Object> loadSTEPlugins(Main view) throws Exception {
        try {
            LinkedHashMap<String, Object> activePlugins = new LinkedHashMap<>();

            // get a NodeList of all plugins specified in the plugin xml
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            File fXmlFile = new File(classloader.getResource("settings.xml").getFile());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("plugin");

            for (int pid = 0; pid < nList.getLength(); pid++) {
                Node nNode = nList.item(pid);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String isActive = eElement.getElementsByTagName("active").item(0).getTextContent();

                    // only load plugin class when plugin is set to active
                    if(isActive.equals("1")) {
                        String pluginName = eElement.getElementsByTagName("name").item(0).getTextContent();
                        String pluginPath = eElement.getElementsByTagName("path").item(0).getTextContent();
                        String hasMenu = eElement.getElementsByTagName("hasMenu").item(0).getTextContent();

                        // the properties of the plugin that gets loaded at the moment
                        LinkedHashMap<String, String> loadedPluginsProps = new LinkedHashMap<>();
                        loadedPluginsProps.put("name", pluginName);
                        loadedPluginsProps.put("hasMenu", hasMenu);

                        // add loaded plugin to the list with properties of all plugins
                        pluginProperties.put(pluginName, loadedPluginsProps);

                        // java reflections load the plugin that should get loaded at the moment and invokes the start method for it
                        Object pluginClass;
                        pluginClass = Class.forName(pluginPath).newInstance();
                        activePlugins.put(pluginName, pluginClass);
                        Method load = pluginClass.getClass().getMethod("loadPlugin", view.getClass());
                        load.invoke(pluginClass, view);
                    }
                }
            }
            return activePlugins;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Returns the properties specified in the plugin.xml file for the given plugin.
     * @param plugin : name of the plugin the properties should be returned of
     * @return LinkedHashMap with all plugin properties
     */
    public static LinkedHashMap<String, String> getPluginProperties(String plugin) {
        return pluginProperties.get(plugin);
    }

} // End of class Plugin
