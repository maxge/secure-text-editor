package plugins.encrypt.views;

// Import STE lib view classes
import controllers.MenuHandler;

// Import plugin controller
import javafx.scene.layout.Region;
import jdk.nashorn.internal.ir.Block;
import plugins.encrypt.controllers.Encrypt;
import plugins.encrypt.controllers.PBEncrypt;
import plugins.encrypt.models.Algorithm;
import plugins.encrypt.models.BlockMode;

// Import needed java libs
import javafx.scene.control.*;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.*;
import plugins.encrypt.models.ContentReader;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * The view class of the Encrypt plugin. Creates the view and handles actions via modifying the view or redirecting to the right controller.
 *
 * @author : Max Geldner
 * @version : 1.4i1
 */
public class EncryptView {

    private Encrypt encrypt;
    private Stage selectionView;
    private Stage pwEntryView;
    private int preferredAlgo = 0;

    /**
     * Adds the submenus for the Encrypt plugin
     * @param menu : the plugin menu created by the ste
     * @param encrypt : The Instance of the encrypt class
     * @return the filled menus that gets handed back to the ste through the plugin main class
     */
    public Menu addSubMenus(Menu menu, Encrypt encrypt) {
        this.encrypt = encrypt;
        EventHandler<ActionEvent> action = handleMenu();

        MenuItem enc = new MenuItem("Save file encrypted");
        enc.setOnAction(action);

        MenuItem ope = new MenuItem("Open encrypted file");
        ope.setOnAction(action);

        menu.getItems().addAll(enc, ope);
        return menu;
    }

    /**
     * Sets up the view for the algorithm selection and renders it.
     * Also makes the newly created stage accessible for this class.
     */
    private void doRenderAlgoSelection() {
        // Create and setup pane
        FlowPane algoSelect = new FlowPane();
        algoSelect.setPadding(new Insets(12, 5, 5, 5));
        algoSelect.setHgap(5);

        // --- Drop Down Menus
        LinkedHashMap<String, ArrayList> content = this.encrypt.getEncryptContents();
        EventHandler<ActionEvent> algoCbAction = handleAlgoBoxChange();
        EventHandler<ActionEvent> modeCbAction = handleModeBoxChange();

        ComboBox algos = new ComboBox();
        ComboBox block = new ComboBox();
        ComboBox padding = new ComboBox();
        algos.setItems(FXCollections.observableArrayList(this.convertAlgorithmsToString(content.get("Algorithms"))));
        algos.getSelectionModel().select(this.preferredAlgo);
        algos.setOnAction(algoCbAction);
        block.setItems(FXCollections.observableArrayList(this.convertBlockModesToString(content.get("BlockModes"))));
        block.getSelectionModel().select(0);
        block.setOnAction(modeCbAction);
        padding.setItems(FXCollections.observableArrayList(content.get("Paddings")));
        padding.getSelectionModel().select(0);

        // --- Key Size Slider and Standard Text
        Slider keySizeSlider = new Slider();
        keySizeSlider.setMin(16);
        keySizeSlider.setMax(32);
        keySizeSlider.setValue(16);
        keySizeSlider.setShowTickLabels(true);
        keySizeSlider.setShowTickMarks(true);
        keySizeSlider.setSnapToTicks(true);
        keySizeSlider.setMajorTickUnit(8);
        keySizeSlider.setMinorTickCount(0);
        keySizeSlider.setPadding(new Insets(10, 0, 0, 0));

        Text defaultKeySize = new Text();
        defaultKeySize.setText("8");

        // If the first algorithm in the list is AES show the slider. If this is not the case show the default key size text
        if(!algos.getItems().get(this.preferredAlgo).toString().equalsIgnoreCase("AES")) {
            keySizeSlider.setVisible(false);
        } else {
            defaultKeySize.setVisible(false);
        }

        // --- PBE Tick Mark and Entry Field
        CheckBox usePBE = new CheckBox();
        EventHandler<ActionEvent> pbeChange = handlePBEMarkChange();
        usePBE.setOnAction(pbeChange);
        Label pwLabel = new Label("Password: ");
        pwLabel.setVisible(false);
        PasswordField password = new PasswordField();
        password.setVisible(false);

        // --- Hashing Drop Down
        ComboBox hashing = new ComboBox();
        hashing.setItems(FXCollections.observableArrayList(content.get("Hashings")));
        hashing.getSelectionModel().select(0);

        // --- Form Submit Button
        Button submit = new Button("Encrypt");
        submit.setStyle("-fx-base: #b6e7c9;");
        EventHandler<ActionEvent> action = handleSubmit();
        submit.setOnAction(action);

        // Add all elements to the pane. Values used are 0 to n
        algoSelect.getChildren().add(new Label("Select algorithm: "));
        algoSelect.getChildren().add(algos);
        algoSelect.getChildren().add(new Label("Select block: "));
        algoSelect.getChildren().add(block);
        algoSelect.getChildren().add(new Label("Select padding: "));
        algoSelect.getChildren().add(padding);     // 5
        algoSelect.getChildren().add(new Label("Keysize (byte): "));
        algoSelect.getChildren().add(keySizeSlider);
        algoSelect.getChildren().add(defaultKeySize);
        algoSelect.getChildren().add(this.createLineBreak());
        algoSelect.getChildren().add(new Label("Password Based Encryption: "));     // 10
        algoSelect.getChildren().add(usePBE);
        algoSelect.getChildren().add(pwLabel);
        algoSelect.getChildren().add(password);
        algoSelect.getChildren().add(this.createLineBreak());
        algoSelect.getChildren().add(new Label("Select hashing method: "));     // 15
        algoSelect.getChildren().add(hashing);
        algoSelect.getChildren().add(this.createLineBreak());
        algoSelect.getChildren().add(submit);
        // n = 18

        // Create, setup and render stage
        Stage stage = new Stage();
        stage.setTitle("Select encryption algorithm");
        stage.setScene(new Scene(algoSelect, 600, 165));
        stage.show();

        this.selectionView = stage;
    }

    /**
     * Sets up the password entry dialogue that gets triggered when pbe is used in decryption.
     * @param openedFile : The file that should be opened. Gets passed to its event handler
     */
    private void doRenderPasswordEntry(String[] openedFile) {
        FlowPane pwEntry = new FlowPane();
        pwEntry.setPadding(new Insets(12, 5, 5, 5));
        pwEntry.setHgap(5);

        // --- PW Entry field
        Label pwLabel = new Label("Password: ");
        PasswordField password = new PasswordField();

        // --- Form Submit Button
        Button submit = new Button("Decrypt");
        submit.setStyle("-fx-base: #b6e7c9;");
        EventHandler<ActionEvent> action = handlePasswordEntryForm(openedFile);
        submit.setOnAction(action);

        pwEntry.getChildren().addAll(pwLabel, password, submit);

        Stage stage = new Stage();
        stage.setTitle("Enter Password for decryption");
        stage.setScene(new Scene(pwEntry, 290, 50));
        stage.show();

        this.pwEntryView = stage;
    }

    // ******************
    // Alert triggers
    // ******************

    /**
     * Generates a warning text for the case that an encryption with noPadding was tried, but the text length didn't match.
     * Passes this text to the warn dialogue rendering method in the main view.
     * @param inputLength : Integer with the length of the input to encrypt
     * @param algorithmBlockSize : Integer with the block size of the algorithm
     */
    public void triggerNoPaddingBlockSizeWarning(int inputLength, int algorithmBlockSize) {
        int res = (int)Math.ceil((double)inputLength / 16) * 16 - inputLength;
        String warningTitle = "Block size is not valid for NoPadding mechanism!";
        String warningText = "Your text length has to be a multiple of " + algorithmBlockSize +" charcaters, but it is "
                            + inputLength + " characters long (includes hashing). "
                            + "Make your text " + res + " characters longer or choose another padding.";
        encrypt.getMainView().showWarnDialogue(encrypt.getPluginName(), warningTitle, warningText);
    }

    /**
     * Dialogue that gets triggered when no Password was entered when using a password based encryption.
     */
    public void triggerNoPWEnteredWarning() {
        String warningTitle = "Error while using Password Based Encryption";
        String warningText = "Please enter a password when using Password based encryption!";
        encrypt.getMainView().showWarnDialogue(encrypt.getPluginName(), warningTitle, warningText);
    }

    /**
     * Dialogue that gets triggered when the wrong password was used while decrypting a password based encrypted file.
     */
    private void triggerWrongPasswordWarning() {
        String warningTitle = "Error while using Password Based Decryption";
        String warningText = "The decryption failed. Please ensure, that you used the right password for the decryption!";
        encrypt.getMainView().showWarnDialogue(encrypt.getPluginName(), warningTitle, warningText);
    }

    /**
     * Dialogue that gets triggered when the wrong password was used while decrypting a password based encrypted file.
     */
    public void triggerWasTampered() {
        String warningTitle = "Your file might have been modified!";
        String warningText = "The hash values of your saved file and the now decrypted file are different. There might have been a modification to this file.";
        encrypt.getMainView().showWarnDialogue(encrypt.getPluginName(), warningTitle, warningText);
    }

    /**
     * Converts a given ArrayList of BlockModes to an ArrayList of Strings.
     * @param blockModes : ArrayList of type BlockMode with all block modes to convert
     * @return ArrayList with the String names of all block modes
     */
    private ArrayList<String> convertBlockModesToString(ArrayList<BlockMode> blockModes) {
        ArrayList<String> bmNames = new ArrayList<>();
        for(BlockMode bm : blockModes) {
            bmNames.add(bm.getName());
        }
        return bmNames;
    }

    /**
     * Converts a given ArrayList of Algorithms to an ArrayList of Strings.
     * @param algos : ArrayList of type Algorithm with all algos to convert
     * @return ArrayList with the String names of all algorithms
     */
    private ArrayList<String> convertAlgorithmsToString(ArrayList<Algorithm> algos) {
        ArrayList<String> algoNames = new ArrayList<>();
        for(Algorithm algo : algos) {
            algoNames.add(algo.getName());
        }
        return algoNames;
    }

    /**
     * Returns a Region Element that can be used as a line break in JavaFX.
     * @return Region : the line break
     */
    private Region createLineBreak() {
        Region lineBreak = new Region();
        lineBreak.setPrefSize(Double.MAX_VALUE, 0.0);
        return lineBreak;
    }

    /**
     * Triggers the entry dialogue for the password.
     * @param openedFile : The file that should be decrypted
     */
    public void triggerPWEnterDialogue(String[] openedFile) {
        this.doRenderPasswordEntry(openedFile);
    }

    // ******************
    // Event Handler
    // ******************

    /**
     * Event handler for menu click events.
     * @return the event
     */
    private EventHandler<ActionEvent> handleMenu() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                MenuItem clicked = (MenuItem) event.getSource();
                String item = clicked.getText();
                MenuHandler menuHandler = new MenuHandler();

                if(item.equalsIgnoreCase("Save file encrypted")) {
                    try {
                        doRenderAlgoSelection();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(item.equalsIgnoreCase("Open encrypted file")) {
                    try {
                        // Go to decrypt AES here and set text of TA to decrypted text
                        String[] openedFile = menuHandler.doOpenFile(false, "");
                        encrypt.decryptSymmetric(openedFile, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    /**
     * Event handler for key size slider or default key size text visibility.
     * @return the event
     */
    private EventHandler<ActionEvent> handleAlgoBoxChange() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                FlowPane pane = (FlowPane) selectionView.getScene().getRoot();
                ComboBox algo = (ComboBox) pane.getChildren().get(1);
                ComboBox blockMode = (ComboBox) pane.getChildren().get(3);
                ComboBox padding = (ComboBox) pane.getChildren().get(5);
                Slider slider = (Slider) pane.getChildren().get(7);
                Text defaultKeySize = (Text) pane.getChildren().get(8);
                Algorithm usedAlgo = encrypt.getAlgorithm(algo.getValue().toString());

                ContentReader content = new ContentReader();
                LinkedHashMap<String, ArrayList> cont = content.getContent();
                // Trigger changes for key length
                if(usedAlgo.getMaxKeyLength() == -1) {
                    slider.setVisible(true);
                    defaultKeySize.setVisible(false);
                } else if(usedAlgo.getMaxKeyLength() != -1) {
                    slider.setVisible(false);
                    defaultKeySize.setVisible(true);
                    defaultKeySize.setText(usedAlgo.getMaxKeyLength() + "");
                }
                // Trigger changes for stream ciphers
                if(usedAlgo.getIsStreamcipher()) {
                    blockMode.setDisable(true);
                    padding.setDisable(true);
                } else {
                    blockMode.setDisable(false);
                    padding.setDisable(false);
                }
                // Trigger changes for available block modes
                if(!usedAlgo.getName().equalsIgnoreCase("AES") && !usedAlgo.getIsStreamcipher()) {
                    ArrayList<String> toShow = new ArrayList<>();
                    for(Object bm : cont.get("BlockModes")) {
                        if(!((BlockMode)bm).getAESOnly()) {
                            toShow.add(((BlockMode) bm).getName());
                        }
                    }
                    blockMode.setItems(FXCollections.observableArrayList(toShow));
                } else {
                    blockMode.setItems(FXCollections.observableArrayList(convertBlockModesToString(cont.get("BlockModes"))));
                }
            }
        };
    }

    /**
     * Event handler for key size slider or default key size text visibility.
     * @return the event
     */
    private EventHandler<ActionEvent> handleModeBoxChange() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                FlowPane pane = (FlowPane) selectionView.getScene().getRoot();
                ComboBox algo = (ComboBox) pane.getChildren().get(1);
                Algorithm usedAlgo = encrypt.getAlgorithm(algo.getValue().toString());
                ComboBox blockMode = (ComboBox) pane.getChildren().get(3);
                ComboBox padding = (ComboBox) pane.getChildren().get(5);
                String blockModeString = (String) blockMode.getValue();
                if(blockModeString == null) {
                    blockMode.getSelectionModel().select(0);
                    blockModeString = (String) blockMode.getValue();
                }
                if(blockModeString != null && !encrypt.getBlockMode(blockModeString).getNeedsPadding() || usedAlgo.getIsStreamcipher()) {
                    padding.setDisable(true);
                    padding.getSelectionModel().select(padding.getItems().indexOf("NoPadding"));
                } else if(blockModeString != null) {
                    padding.setDisable(false);
                }
            }
        };
    }

    /**
     * Event handler for pressing the submit button in the selection form.
     * @return the event
     */
    private EventHandler<ActionEvent> handleSubmit() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                PBEncrypt pbeEncrypt = new PBEncrypt(encrypt);
                FlowPane pane = (FlowPane) selectionView.getScene().getRoot();
                ComboBox algo = (ComboBox) pane.getChildren().get(1);
                ComboBox mode = (ComboBox) pane.getChildren().get(3);
                ComboBox padding = (ComboBox) pane.getChildren().get(5);
                Slider slider = (Slider) pane.getChildren().get(7);
                CheckBox usePBE = (CheckBox) pane.getChildren().get(11);
                PasswordField pwField = (PasswordField) pane.getChildren().get(13);
                ComboBox hashing = (ComboBox) pane.getChildren().get(16);
                String[] encryptSuccess = null;
                Algorithm usedAlgo = encrypt.getAlgorithm(algo.getValue().toString());
                if(usedAlgo.getIsStreamcipher()) {
                    if(usePBE.isSelected() && !pwField.getText().equals("")) {
                        encryptSuccess = pbeEncrypt.encryptSymmetric(algo.getValue().toString(), (int)slider.getValue(), pwField.getText(), encrypt.getMainView().getTextAreaContent(), hashing.getValue().toString(), false);
                    } else if(usePBE.isSelected() && pwField.getText().equals(""))  {
                        triggerNoPWEnteredWarning();
                    } else if(!usePBE.isSelected()) {
                        encryptSuccess = encrypt.encryptSymmetric(algo.getValue().toString(), null, null, (int)slider.getValue(), encrypt.getMainView().getTextAreaContent(), hashing.getValue().toString(), false);
                    }
                } else {
                    int keyLength;
                    if(encrypt.getAlgorithm(algo.getValue().toString()).getMaxKeyLength() == -1) {
                        keyLength = (int)slider.getValue();
                    } else {
                        keyLength = encrypt.getAlgorithm(algo.getValue().toString()).getMaxKeyLength();
                    }
                    if(usePBE.isSelected() && !pwField.getText().equals("")) {
                        encryptSuccess = pbeEncrypt.encryptSymmetric(algo.getValue().toString(), keyLength, pwField.getText(), encrypt.getMainView().getTextAreaContent(), hashing.getValue().toString(), false);
                    } else if(usePBE.isSelected() && pwField.getText().equals(""))  {
                        triggerNoPWEnteredWarning();
                    } else if(!usePBE.isSelected()) {
                        encryptSuccess = encrypt.encryptSymmetric(algo.getValue().toString(), mode.getValue().toString(), padding.getValue().toString(), keyLength, encrypt.getMainView().getTextAreaContent(), hashing.getValue().toString(), false);
                    }
                }
                if(encryptSuccess.length != 0) {
                    selectionView.close();
                    encrypt.getMainView().setTitleSaved();
                }
            }
        };
    }

    /**
     * Event handler for key size slider or default key size text visibility.
     * @return the event
     */
    private EventHandler<ActionEvent> handlePBEMarkChange() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                FlowPane pane = (FlowPane) selectionView.getScene().getRoot();
                ComboBox mode = (ComboBox) pane.getChildren().get(3);
                ComboBox padding = (ComboBox) pane.getChildren().get(5);
                CheckBox usePBE = (CheckBox) pane.getChildren().get(11);
                Label pwLabel = (Label) pane.getChildren().get(12);
                PasswordField pwField = (PasswordField) pane.getChildren().get(13);
                pwLabel.setVisible(usePBE.isSelected());
                pwField.setVisible(usePBE.isSelected());
                mode.setDisable(usePBE.isSelected());
                padding.setDisable(usePBE.isSelected());
            }
        };
    }

    /**
     * Event handler for password entry at decryption
     * @param openedFile : String Array with the data of the file that should get opened
     * @return the event
     */
    private EventHandler<ActionEvent> handlePasswordEntryForm(String[] openedFile) {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                FlowPane pane = (FlowPane) pwEntryView.getScene().getRoot();
                PasswordField pwField = (PasswordField) pane.getChildren().get(1);
                PBEncrypt pbEncrypt = new PBEncrypt(encrypt);
                if(pwField.getText() != "") {
                    String result = pbEncrypt.decryptSymmetric(openedFile, false, pwField.getText());
                    if(result.equalsIgnoreCase("")) {
                        triggerWrongPasswordWarning();
                    } else {
                        pwEntryView.close();
                    }
                }
            }
        };
    }

} // End of class EncryptView