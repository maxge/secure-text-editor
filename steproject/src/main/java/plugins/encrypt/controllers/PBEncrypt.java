package plugins.encrypt.controllers;

import plugins.encrypt.models.Algorithm;
import plugins.encrypt.models.BlockMode;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/**
 * PBE Class for the Encrypt plugin of the STE. This plugin can handle symmetric encryption algorithms including DES and AES with passwords.
 * It also is capable different padding algorithms including NoPadding, ZeroBytePadding and PKCS7 padding. It can decrypt an input string with different block
 * cipher modes. Also Stream based ciphers are implemented (RC4).
 *
 * The Plugin uses the LegionOfTheBouncyCastle encryption library.
 *
 * @author : Max Geldner
 * @version : 1.0i5
 */
public class PBEncrypt {

    private Encrypt encrypt;

    public PBEncrypt(Encrypt _encrypt) {
        this.encrypt = _encrypt;
    }

    /**
     * Method for symmetric encryption with passwords. Compares to encrypt.encryptSymmetric()-Method.
     * @param algorithm : String with the name of the used algorithm. Only is used for creating an algorithm with the Algorithm-Class
     * @param keyLength : Integer with the key length. User or Views enter this key length
     * @param password : The Password that should get used fo the encryption
     * @param text : The text that should get encrypted
     * @param hashing : String of the hashing algorithm that should get sued to hash the plain text
     * @param skipSave : boolean, true if the save of the file should get skipped and only the encrypted text should get returned
     * @return String[] : With the metadata of the encryption process on the first position and the encrypted text at the second position
     */
    public String[] encryptSymmetric(String algorithm, int keyLength, String password, String text, String hashing, boolean skipSave) {
        Cipher cipher;
        byte[] input = text.getBytes(StandardCharsets.UTF_8);
        IvParameterSpec ivSpec;
        String metadata = "PW|";
        String cipherInstanceInfo;
        int iterationCount = 124000;
        try {
            // Get the given Block-Mode and Algorithm
            BlockMode usedBM = this.encrypt.getBlockMode("CBC");
            Algorithm usedAlgo = this.encrypt.getAlgorithm(algorithm);
            MessageDigest hash = MessageDigest.getInstance(hashing, "BC");

            if(!usedAlgo.getIsStreamcipher()) {
                cipherInstanceInfo = usedAlgo.getName() + "/" + usedBM.getName() + "/" + "PKCS7Padding";
                cipher = Cipher.getInstance(cipherInstanceInfo, "BC");
            } else {
                cipherInstanceInfo = usedAlgo.getName();
                cipher = Cipher.getInstance(cipherInstanceInfo, "BC");
            }

            byte[] salt = this.generateSalt(cipher.getBlockSize());
            SecretKey key = this.generateKey(usedAlgo, keyLength, password.toCharArray(), salt, iterationCount);
            metadata += this.encrypt.toHex(salt) + "|";
            metadata += iterationCount + "|";

            // Cipher has to be initiated with IV when CBC or similar
            if(usedBM != null && usedBM.getNeedsIV()) {
                byte[] ivBytes = this.encrypt.generateIVBytes(usedBM.getNeedsCounter(), cipher.getBlockSize());
                ivSpec = new IvParameterSpec(ivBytes);
                // save the iv in the encrypted file for later use
                metadata += this.encrypt.toHex(ivBytes) + "|";
                cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, key);
            }

            // Encryption
            byte[] cipherText = new byte[cipher.getOutputSize(input.length + hash.getDigestLength())];
            int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
            hash.update(input);
            byte[] digest = hash.digest();
            cipher.doFinal(digest, 0, hash.getDigestLength(), cipherText, ctLength);

            // save the cipher text and the used algorithm, block mode and padding into the encrypted text file
            String result = this.encrypt.toHex(cipherText);
            metadata += cipherInstanceInfo + "|";
            metadata += keyLength + "|";
            metadata += hashing + "|";
            String[] resultArray = {metadata, result};

            try {
                if(!skipSave) {
                    this.encrypt.saveEncryptionFiles(resultArray, null, true);
                }
                return resultArray;
            } catch(NullPointerException e) {
                System.out.println("No file was selected!");
                return new String[0];
            } catch(Exception e) {
                e.printStackTrace();
                return new String[0];
            }
        } catch (NoSuchAlgorithmException |NoSuchProviderException |NoSuchPaddingException |
                InvalidKeyException |ShortBufferException |BadPaddingException |InvalidAlgorithmParameterException |IllegalBlockSizeException e) {
            e.printStackTrace();
            return new String[0];
        }
    }

    /**
     * Method for decrypting data that is contained in the second position of the given openedFile Array.
     * Compares to encrypt.decryptSymmetric()-Method
     * @param openedFile : An array with the file path and file data of the opened file
     * @param skipSetText : Boolean, true if the setting of the text of the ste text field should be skipped, mostly used for testing
     * @param password : String of the password that should get used for the decryption (user input)
     * @return String : With the decrypted and processed text that should get shown on the text editor text field
     */
    public String decryptSymmetric(String[] openedFile, boolean skipSetText, String password) {
        Cipher cipher;
        boolean needsIV = false;
        boolean isStreamCipher = false;
        try {
            String[] fileData = openedFile[1].split("\\|");
            byte[] salt = this.encrypt.toByte(fileData[1]);
            int itCount = Integer.parseInt(fileData[2]);
            String[] usedParams = fileData[fileData.length - 4].split("/");
            int keyLength = Integer.parseInt(fileData[fileData.length - 3]);
            String hashing = fileData[fileData.length - 2];
            byte[] encryptedText = this.encrypt.toByte(fileData[fileData.length - 1]);
            String algo;
            if(usedParams.length > 1) {
                needsIV = this.encrypt.getBlockMode(usedParams[usedParams.length - 2]).getNeedsIV();
                algo = usedParams[usedParams.length - 3];
            } else {
                isStreamCipher = this.encrypt.getAlgorithm(usedParams[usedParams.length - 1]).getIsStreamcipher();
                algo = usedParams[usedParams.length - 1];
            }

            // generate Key
            SecretKey originalKey = this.generateKey(this.encrypt.getAlgorithm(algo), keyLength, password.toCharArray(), salt, itCount);

            if(!isStreamCipher) {
                cipher = Cipher.getInstance(fileData[fileData.length - 4], "BC");
            } else {
                cipher = Cipher.getInstance(usedParams[0], "BC");
            }

            // Cipher has to be initiated with IV if using CBC or similar
            if(needsIV) {
                byte[] ivBytes = this.encrypt.toByte(fileData[fileData.length - 5]);
                IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
                cipher.init(Cipher.DECRYPT_MODE, originalKey, ivSpec);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, originalKey);
            }
            byte[] plainText = new byte[cipher.getOutputSize(encryptedText.length)];
            int ptLength = cipher.update(encryptedText, 0, encryptedText.length, plainText, 0);
            cipher.doFinal(plainText, ptLength);

            int counter = 0;
            for(int i = plainText.length - 1; i >= 0; i--) {
                if(plainText[i] != 0) {
                    counter = i + 1;
                    break;
                }
            }
            byte[] newPlain = new byte[counter];
            System.arraycopy(plainText, 0, newPlain, 0, newPlain.length);

            byte[] textToShow = Arrays.copyOfRange(newPlain, 0, this.encrypt.getMessageLength(hashing, newPlain));
            if(this.encrypt.hashAndCompare(hashing, newPlain)) {
                if (!skipSetText) {
                    this.encrypt.getMainView().setTextAreaContent(new String(textToShow, Charset.defaultCharset()));
                }
                return new String(textToShow, Charset.defaultCharset());
            } else {
                this.encrypt.getEncryptionView().triggerWasTampered();
                return "";
            }
        } catch(NoSuchAlgorithmException|NoSuchProviderException|NoSuchPaddingException|InvalidKeyException|
                ShortBufferException|IllegalBlockSizeException|BadPaddingException|InvalidAlgorithmParameterException e) {
            e.getMessage();
            return "";
        }
    }

    /**
     * Generates a secret key object with given parameters. This method can be sued for en- and decryption. The key should be generated
     * in the same way on both usages. All parameters for this method on decryption are given in the decryption file.
     * @param usedAlgo : An Algorithm object that specifies the used algorithm
     * @param keyLength : The length of the key that should be genereated
     * @param password : The password used for the decryption (char[] instead of string)
     * @param salt : The salt that is generated in the method generateSalt()
     * @param iterationCount : The iteration count given in the encryption method or the decryption file
     * @return SecretKey generated from the given parameters
     */
    private SecretKey generateKey(Algorithm usedAlgo, int keyLength, char[] password, byte[] salt, int iterationCount) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(this.getPreparedKeyGenSpec(usedAlgo.getName()), "BC");
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterationCount, keyLength);
            return factory.generateSecret(spec);
        } catch (NoSuchAlgorithmException|InvalidKeySpecException|NoSuchProviderException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Generates a random salt with a given length.
     * @param length : The length the salt should have. If length is 0 or less, the length will be set to 16
     * @return byte[] : with the generated salt
     */
    private byte[] generateSalt(int length) {
        if(length < 1) {
            length = 16;
        }
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[length];
        random.nextBytes(salt);
        return salt;
    }

    /**
     * Converts the simple algorithm names into names used for password based encryption.
     * @param algorithm : The algorithm that should get converted
     * @return String : with the converted algorithm name
     */
    private String getPreparedKeyGenSpec(String algorithm) {
        if(algorithm.equalsIgnoreCase("AES")) {
            return "PBEWithSHAAnd128BitAES-CBC-BC";
        } else if(algorithm.equalsIgnoreCase("DES")) {
            return "PBEWithMD5AndDES";
        } else if(algorithm.equalsIgnoreCase("ARC4")) {
            return "PBEWithSHAAnd40BitRC4";
        }
        return algorithm;
    }

} // End of class PBEncrypt
