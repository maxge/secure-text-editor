package plugins.encrypt.controllers;

// Import STE lib classes
import controllers.MenuHandler;
import plugins.encrypt.models.Algorithm;
import plugins.encrypt.models.BlockMode;
import plugins.encrypt.models.ContentReader;
import views.Main;

// Import plugin view
import plugins.encrypt.views.EncryptView;

// Import needed java libs
import javafx.scene.control.Menu;
import controllers.Plugin;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

/**
 * Encrypt plugin for the STE. This plugin can handle symmetric encryption algorithms including DES and AES. It also is capable of three
 * different padding algorithms including NoPadding, ZeroBytePadding and PKCS7 padding. It can decrypt an input string with different block
 * cipher modes. Also Stream based ciphers are implemented (RC4).
 *
 * The Plugin uses the LegionOfTheBouncyCastle encryption library.
 *
 * @author : Max Geldner
 * @version : 1.3i1
 */
public class Encrypt extends Plugin {

    // The view of the encrypt plugin where the user selects all parameters
    private EncryptView encryptionView;
    // The main view of the text editor used for view modifications
    private Main steView;
    // The Menu Handler class used for file stream actions
    private MenuHandler menuHandler;
    // The name of the pluign
    private String pluginName = "Encrypt Plugin";
    // The digits used for the hex encoding
    private String digits = "0123456789abcdef";
    // The path of the key files
    private String keyPath = "";
    // All available BlockModes in the encrypt plugin with their properties
    private LinkedHashMap<String, BlockMode> availableBMs;
    // All available Algorithms in the encrypt plugin with their properties
    private LinkedHashMap<String, Algorithm> availableAlgos;

    public Encrypt() {
        super();
    }

    @Override
    public void loadPlugin(Main view) {
        this.steView = view;
        this.encryptionView  = new EncryptView();
        this.menuHandler = new MenuHandler();

        this.availableBMs = new LinkedHashMap<>();
        this.availableAlgos = new LinkedHashMap<>();
        this.initializeBlockModes();
        this.initializeAlgorithms();
        this.keyPath = this.readKeyPath();
    }

    @Override
    public Menu addSubMenus(Menu menu) {
        return this.encryptionView.addSubMenus(menu, this);
    }

    /**
     * Read the settings.xml file and find out the path the user specified to store the key files at.
     * @return String of the path in the XML file
     */
    private String readKeyPath() {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            File fXmlFile = new File(classloader.getResource("settings.xml").getFile());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("encrypt");
            Element eElement = (Element) nList.item(0);
            return eElement.getElementsByTagName("keySavePath").item(0).getTextContent();
        } catch (ParserConfigurationException|SAXException|IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Method for symmetric encryption. Contains all calls for encryption with and without an IV, different key lengths
     * encryption with stream ciphers and block ciphers. Creates its own BlockModes and Algorithms from the given strings
     * and uses them for decisions in the encryption. This method also calls methods for saving the encrypted file.
     * @param algorithm String : The name of the used algorithm. Only is used for creating an algorithm with the Algorithm-Class
     * @param blockMode String : The name of the used block mode. Also is there for creating a block mode from the BlockMode-Class
     * @param padding String : The used padding. Is used as a raw string for the cipher creation
     * @param keyLength Integer : The key length. User or Views enter this key length
     * @param text String : The text that should get encrypted
     * @param hashing String : The hashing algorithm that should get sued to hash the plain text
     * @param skipSave boolean : True if the save of the file should get skipped and only the encrypted text should get returned
     * @return String[] : With the metadata of the encryption process on the first position and the encrypted text at the second position
     */
    public String[] encryptSymmetric(String algorithm, String blockMode, String padding, int keyLength, String text, String hashing, boolean skipSave) {
        Cipher cipher = null;
        byte[] input = text.getBytes(StandardCharsets.UTF_8);
        int outputSize = 0;
        IvParameterSpec ivSpec;
        String metadata = "";
        String cipherInstanceInfo;
        try {
            // Get the given Block-Mode and Algorithm
            BlockMode usedBM = this.availableBMs.get(blockMode);
            Algorithm usedAlgo = this.availableAlgos.get(algorithm);
            MessageDigest hash = MessageDigest.getInstance(hashing, "BC");

            if(usedBM != null && !usedBM.getNeedsPadding()) {
                padding = "NoPadding";
            }

            if(!usedAlgo.getIsStreamcipher()) {
                cipherInstanceInfo = usedAlgo.getName() + "/" + usedBM.getName() + "/" + padding;
                cipher = Cipher.getInstance(cipherInstanceInfo, "BC");
            } else {
                cipherInstanceInfo = usedAlgo.getName();
                cipher = Cipher.getInstance(cipherInstanceInfo, "BC");
            }

            SecretKeySpec key = new SecretKeySpec(this.generateKeyBytes(usedAlgo, keyLength), algorithm);

            // Cipher has to be initiated with IV when CBC or similar
            if(usedBM != null && usedBM.getNeedsIV()) {
                byte[] ivBytes = this.generateIVBytes(usedBM.getNeedsCounter(), cipher.getBlockSize());
                ivSpec = new IvParameterSpec(ivBytes);
                // save the iv in the encrypted file for later use
                metadata += this.toHex(ivBytes) + "|";
                cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, key);
            }

            // Encryption
            outputSize = cipher.getOutputSize(input.length + hash.getDigestLength());
            byte[] cipherText = new byte[outputSize];
            int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
            hash.update(input);
            byte[] digest = hash.digest();
            cipher.doFinal(digest, 0, hash.getDigestLength(), cipherText, ctLength);

            // save the cipher text and the used algorithm, block mode and padding into the encrypted text file
            String result = this.toHex(cipherText);
            metadata += cipherInstanceInfo + "|";
            metadata += hashing + "|";
            String[] resultArray = {metadata, result};

            try {
                if(!skipSave) {
                    this.saveEncryptionFiles(resultArray, key, false);
                }
                return resultArray;
            } catch(NullPointerException e) {
                System.out.println("No file was selected!");
                return new String[0];
            } catch(Exception e) {
                e.printStackTrace();
                return new String[0];
            }
        } catch (NoSuchAlgorithmException|NoSuchProviderException|NoSuchPaddingException|
                 InvalidKeyException|ShortBufferException|BadPaddingException|InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return new String[0];
        } catch (IllegalBlockSizeException e) {
            if(padding.equalsIgnoreCase("NoPadding")) {
                encryptionView.triggerNoPaddingBlockSizeWarning(outputSize, cipher.getBlockSize());
            } else {
                e.printStackTrace();
            }
            return new String[0];
        }
    }

    /**
     * Saves the file with the metadata and the cipher text to a user specified folder (via file chooser) and the key file to
     * the specified key file folder.
     * @param resultArray String[] : The result String Array from the encryption process with metadata and cipertext in it
     * @param key SecretKeySpec : A SecretKeySpec Object containing the key that got used encrypting the result Array contents
     * @param skipKeyFile Boolean : True if the save of the keyfile should be skipped. This is mostly used for automated testing
     */
    protected void saveEncryptionFiles(String[] resultArray, SecretKeySpec key, boolean skipKeyFile) {
        String savedFile = this.menuHandler.doSaveFile(resultArray, false, "");
        if(!skipKeyFile) {
            // save the key an the path to the file in an extra file that should be kept confidential
            String[] pathArray = savedFile.split(Pattern.quote("\\"));
            String stringKey = this.toHex(key.getEncoded());
            String[] keydata = {stringKey};
            this.menuHandler.doSaveFile(keydata, true, this.keyPath + pathArray[pathArray.length - 1] + ".kd");
        }
    }

    /**
     * Method for decrypting data that is contained in the second position of the given openedFile Array.
     * Can decrypt text with an IV, depending if one is given in the opened file. Also can do decryption with stream ciphers.
     * @param openedFile String[] : An array with the file path and file data of the opened file
     * @param skipSetText Boolean : True if the setting of the text of the ste text field should be skipped, mostly used for testing
     * @return String : with the processed text that should be shown on the text editor
     */
    public String decryptSymmetric(String[] openedFile, boolean skipSetText) {
        Cipher cipher;
        boolean needsIV = false;
        boolean isStreamCipher = false;
        try {
            String openedFilePath = openedFile[0];
            String[] fileData = openedFile[1].split("\\|");
            if(fileData[0].equalsIgnoreCase("PW")) {
                this.encryptionView.triggerPWEnterDialogue(openedFile);
                return "";
            }
            byte[] encryptedText = this.toByte(fileData[fileData.length - 1]);
            String[] openedFileName = openedFilePath.split(Pattern.quote("\\"));
            String[] usedParams = fileData[fileData.length - 3].split("\\/");
            String usedParamsChained = fileData[fileData.length - 3];
            String hashing = fileData[fileData.length - 2];
            if(usedParams.length > 1) {
                needsIV = this.availableBMs.get(usedParams[usedParams.length - 2]).getNeedsIV();
            } else {
                isStreamCipher = this.availableAlgos.get(usedParams[0]).getIsStreamcipher();
            }

            String[] keyFile = this.menuHandler.doOpenFile(true,  this.keyPath + openedFileName[openedFileName.length - 1] + ".kd");

            byte[] key = this.toByte(keyFile[1]);
            SecretKey originalKey = new SecretKeySpec(key, 0,key.length, keyFile[1]);
            if(!isStreamCipher) {
                cipher = Cipher.getInstance(usedParamsChained, "BC");
            } else {
                cipher = Cipher.getInstance(usedParams[0], "BC");
            }

            // Cipher has to be initiated with IV if using CBC or similar
            if(needsIV) {
                byte[] ivBytes = this.toByte(fileData[fileData.length - 4]);
                IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
                cipher.init(Cipher.DECRYPT_MODE, originalKey, ivSpec);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, originalKey);
            }
            byte[] plainText = new byte[cipher.getOutputSize(encryptedText.length)];
            int ptLength = cipher.update(encryptedText, 0, encryptedText.length, plainText, 0);
            cipher.doFinal(plainText, ptLength);

            int counter = 0;
            for(int i = plainText.length - 1; i >= 0; i--) {
                if(plainText[i] != 0) {
                    counter = i + 1;
                    break;
                }
            }

            byte[] newPlain = new byte[counter];
            System.arraycopy(plainText, 0, newPlain, 0, newPlain.length);

            byte[] textToShow = Arrays.copyOfRange(newPlain, 0, this.getMessageLength(hashing, newPlain));
            if(this.hashAndCompare(hashing, newPlain)) {
                if(!skipSetText) {
                    this.steView.setTextAreaContent(new String(textToShow, Charset.defaultCharset()));
                }
                return new String(textToShow, Charset.defaultCharset());
            } else {
                this.encryptionView.triggerWasTampered();
                this.steView.setTextAreaContent(new String(textToShow, Charset.defaultCharset()));
                return "";
            }
        } catch(NoSuchAlgorithmException|NoSuchProviderException|NoSuchPaddingException|InvalidKeyException|
                ShortBufferException|IllegalBlockSizeException|BadPaddingException|InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Hashes a given text and compares it to the hash value associated with the text in before. Than the two hash values get compared.
     * @param algorithm String : The hashing algorithm name
     * @param toCompare byte[] : The text to hash with the former hash value attached to it
     * @return Boolean : true if the two hash values are the same, false if not
     */
    public boolean hashAndCompare(String algorithm, byte[] toCompare) {
        MessageDigest hash;
        try {
            hash = MessageDigest.getInstance(algorithm, "BC");
        } catch (NoSuchAlgorithmException|NoSuchProviderException e) {
            e.printStackTrace();
            return false;
        }
        int messageLength = this.getMessageLength(algorithm, toCompare);
        hash.update(toCompare, 0, messageLength);
        byte[] messageHash = new byte[hash.getDigestLength()];
        System.arraycopy(toCompare, messageLength, messageHash, 0, messageHash.length);
        return MessageDigest.isEqual(hash.digest(), messageHash);
    }

    /**
     * Returns the length of the message when a message with a attached hashvalue is given.
     * @param algorithm String : The hashing algorithm name
     * @param message byte[] : The byte Array with the message and the attached hash value
     * @return Integer : The initial message length
     */
    public int getMessageLength(String algorithm, byte[] message) {
        MessageDigest hash;
        try {
            hash = MessageDigest.getInstance(algorithm, "BC");
            return message.length - hash.getDigestLength();
        } catch (NoSuchAlgorithmException|NoSuchProviderException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Method to convert a given byte array into a hex string.
     * @param data byte[] : The data that should be converted
     * @return String : with the converted hex string
     */
    protected String toHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int	v = data[i] & 0xff;
            buf.append(this.digits.charAt(v >> 4));
            buf.append(this.digits.charAt(v & 0xf));
        }
        return buf.toString();
    }

    /**
     * Method to convert a given hex string into a byte array.
     * @param input String : The hex data
     * @return byte[] : the converted byte array from the hex string
     */
    protected byte[] toByte(String input) {
        int len = input.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4) + Character.digit(input.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Method checks for the four weak keys that are present in DES.
     * @param hexKey String : The key to check in hex format
     * @return true : When the given key matched with one of the weak keys
     *         false : When the given key didn't match with one of the weak keys
     */
    private boolean isWeakKey(String hexKey) {
        String[] weakKeys = {"0101010101010101", "FEFEFEFEFEFEFEFE", "E0E0E0E0F1F1F1F1", "1F1F1F1F0E0E0E0E"};
        if(Arrays.asList(weakKeys).contains(hexKey)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fills the class variable availableBMs with a list of BlockMode objects depending on what block modes are
     * defined in the encryptContents.xml file and are read by the content reader class.
     */
    private void initializeBlockModes() {
        ArrayList<BlockMode> bms = this.getEncryptContents().get("BlockModes");
        for(int i = 0; i < bms.size(); i++) {
            this.availableBMs.put(bms.get(i).getName(), bms.get(i));
        }
    }

    /**
     * Fills the class variable availableAlgos with a list of Algorithm objects depending on what algorithms are
     * defined in the encryptContents.xml file and are read by the content reader class.
     */
    private void initializeAlgorithms() {
        ArrayList<Algorithm> algos = this.getEncryptContents().get("Algorithms");
        for(int i = 0; i < algos.size(); i++) {
            this.availableAlgos.put(algos.get(i).getName(), algos.get(i));
        }
    }

    /**
     * Generates a random byte array with the SecureRandom class depending on the given parameters.
     * @param withCounter Boolean : If the iv needs to have a nonce part and a counter part (used i.e. for CTR and GCM)
     * @param cipherBlockSize Integer : The block size of the algorithm. IV has the length of the block size
     * @return byte[] : array with the generated IV bytes
     */
    protected byte[] generateIVBytes(boolean withCounter, int cipherBlockSize) {
        SecureRandom random = new SecureRandom();
        byte[] ivBytes;
        if(withCounter) {
            byte[] nonceBytes = new byte[cipherBlockSize / 2];
            random.nextBytes(nonceBytes);
            // Fill all bytes with 0 bytes, the last bytes holds the initial message number (always a 1 byte)
            byte[] counterBytes = new byte[cipherBlockSize / 2];
            Arrays.fill(counterBytes, (byte) 0x00);
            counterBytes[counterBytes.length - 1] = (byte) 0x01;
            // Concat the two arrays
            ivBytes = new byte[nonceBytes.length + counterBytes.length];
            System.arraycopy(nonceBytes, 0, ivBytes, 0, nonceBytes.length);
            System.arraycopy(counterBytes, 0, ivBytes, nonceBytes.length, counterBytes.length);
        } else {
            ivBytes = new byte[cipherBlockSize];
            random.nextBytes(ivBytes);
        }
        return ivBytes;
    }

    /**
     * Generates a random byte array that is later used when creating the key. Also checks for weak Keys when using the DES algorithm.
     * @param usedAlgo Algorithm : The used algorithm in the encryption process
     * @param keyLength Integer : The wished key length. Determines the size of the returned byte array
     * @return byte[] : array with the generated key bytes and the length of the key length attribute
     */
    private byte[] generateKeyBytes(Algorithm usedAlgo, int keyLength) {
        byte[] keyBytes = new byte[keyLength];
        // generate random keys with a customized key size, uses SecureRandom class.
        // Also checks for weak keys in DES and regenerates keys when they were weak.
        if(!usedAlgo.getName().equals("DES")) {
            SecureRandom random = new SecureRandom();
            keyBytes = new byte[keyLength];
            random.nextBytes(keyBytes);
        } else {
            // key length should be 8 by default on DES. Just to be sure, also set it to 8 in backend to prevent frontend errors.
            keyLength = 8;
            boolean regenerate = true;
            while (regenerate) {
                SecureRandom random = new SecureRandom();
                keyBytes = new byte[keyLength];
                random.nextBytes(keyBytes);
                regenerate = this.isWeakKey(this.toHex(keyBytes));
            }
        }
        return keyBytes;
    }

    public Main getMainView() {
        return this.steView;
    }

    public String getPluginName() {
        return this.pluginName;
    }

    public LinkedHashMap<String, ArrayList> getEncryptContents() {
        ContentReader cr = new ContentReader();
        return cr.getContent();
    }

    public BlockMode getBlockMode(String name) {
        this.initializeBlockModes();
        return this.availableBMs.get(name);
    }

    public Algorithm getAlgorithm(String name) {
        this.initializeAlgorithms();
        return this.availableAlgos.get(name);
    }

    public EncryptView getEncryptionView() {
        return this.encryptionView;
    }
} // End class Encrypt