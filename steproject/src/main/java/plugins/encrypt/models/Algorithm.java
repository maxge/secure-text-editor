package plugins.encrypt.models;

public class Algorithm {

    // name of the created algorithm
    private String name;
    // whether the created algorithm is a stream cipher or not
    private boolean isStreamcipher;
    // the maximum key length of the cipher. -1 if the cipher has no single maximum key length
    private int maxKeyLength;

    public Algorithm(String _name, boolean _isStreamcipher, int _maxKeyLength) {
        this.name = _name;
        this.isStreamcipher = _isStreamcipher;
        this.maxKeyLength = _maxKeyLength;
    }

    public String getName() {
        return this.name;
    }

    public boolean getIsStreamcipher() {
        return this.isStreamcipher;
    }

    public int getMaxKeyLength() {
        return this.maxKeyLength;
    }
}
