package plugins.encrypt.models;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * This class reads the encryptContents XML file and returns its contents if the method getContent() is called.
 *
 * @author : Max Geldner
 * @version : 1.0i1
 */
public class ContentReader {

    // LinkedHashMap with the category as String key and an ArrayList with the entries in the categories as value
    private LinkedHashMap<String, ArrayList> fileContent;

    public ContentReader() {
        this.readInContents();
    }

    /**
     * Fills a the classes linked hashmap with all algos, modes and paddings that are defined in the encryptContents.xml file.
     */
    private void readInContents() {
        LinkedHashMap<String, ArrayList> content = new LinkedHashMap<>();
        try {
            // get a NodeList of all plugins specified in the plugin xml
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            File fXmlFile = new File(classloader.getResource("encryptContents.xml").getFile());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            content.put("Algorithms", this.getAlgos(doc.getElementsByTagName("algo")));
            content.put("BlockModes", this.getModes(doc.getElementsByTagName("mode")));
            content.put("Paddings", this.getPaddings(doc.getElementsByTagName("padding")));
            content.put("Hashings", this.getHashes(doc.getElementsByTagName("hash")));
        } catch (ParserConfigurationException|SAXException|IOException e) {
            e.printStackTrace();
        }
        this.fileContent = content;
    }

    /**
     * Fill an array list with all given algorithm in the nodelist that originates from the xml-file.
     * @param algos : NodeList with all algos defined by the algo tag
     * @return ArrayList with all algorithms. Has type Algorithm
     */
    private ArrayList<Algorithm> getAlgos(NodeList algos) {
        ArrayList<Algorithm> allAlgos = new ArrayList<>();
        for (int pid = 0; pid < algos.getLength(); pid++) {
            Node nNode = algos.item(pid);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String name = eElement.getTextContent();
                boolean isStreamCipher = eElement.getAttribute("streamcipher").equalsIgnoreCase("true") ? true : false;
                int maxKeyLength = Integer.parseInt(eElement.getAttribute("maxKeyLength"));
                allAlgos.add(new Algorithm(name, isStreamCipher, maxKeyLength));
            }
        }
        return allAlgos;
    }

    /**
     * Fill an array list with all given modes in the nodelist that originates from the xml-file.
     * @param modes : NodeList with all modes defined by the modes tag
     * @return ArrayList with all modes. Has type Mode
     */
    private ArrayList<BlockMode> getModes(NodeList modes) {
        ArrayList<BlockMode> allModes = new ArrayList<>();
        for (int pid = 0; pid < modes.getLength(); pid++) {
            Node nNode = modes.item(pid);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String name = eElement.getTextContent();
                boolean needsIV = eElement.getAttribute("needsIv").equalsIgnoreCase("false") ? false : true;
                boolean needsPad = eElement.getAttribute("needsPad").equalsIgnoreCase("false") ? false : true;
                boolean needsCounter = eElement.getAttribute("needsCounter").equalsIgnoreCase("false") ? false : true;
                boolean aesonly = eElement.getAttribute("aesonly").equalsIgnoreCase("false") ? false : true;
                allModes.add(new BlockMode(name, needsIV, needsPad, needsCounter, aesonly));
            }
        }
        return allModes;
    }

    /**
     * Fill an array list with all given paddings in the nodelist that originates from the xml-file.
     * @param paddings : NodeList with all paddings defined by the padding tag
     * @return ArrayList with all paddings. Has type String
     */
    private ArrayList<String> getPaddings(NodeList paddings) {
        ArrayList<String> allPaddings = new ArrayList<>();
        for (int pid = 0; pid < paddings.getLength(); pid++) {
            Node nNode = paddings.item(pid);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String name = eElement.getTextContent();
                allPaddings.add(name);
            }
        }
        return allPaddings;
    }

    /**
     * Fill an array list with all given hashing algorithms in the nodelist that originates from the xml-file.
     * @param hashes : NodeList with all algos defined by the hash tag
     * @return ArrayList with all hashings. Has type String
     */
    private ArrayList<String> getHashes(NodeList hashes) {
        ArrayList<String> allHashes = new ArrayList<>();
        for (int pid = 0; pid < hashes.getLength(); pid++) {
            Node nNode = hashes.item(pid);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String name = eElement.getTextContent();
                allHashes.add(name);
            }
        }
        return allHashes;
    }

    /**
     * Gets all contents of the xml-file. Accessor method for all classes that use this class.
     * @return LinkedHashMap with String category as key and ArrayLists of type Algorithm, Mode or String as value
     */
    public LinkedHashMap<String, ArrayList> getContent() {
        return this.fileContent;
    }
}