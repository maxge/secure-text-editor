package plugins.encrypt.models;

public class BlockMode {

    // Name of the Block mode
    private String name;
    // If the block mode needs an IV to work
    private boolean needsIV;
    // If the block mode needs padding to work
    private boolean needsPadding;
    // If the block mode needs a counter in the iv
    private boolean needsCounter;
    // If the block mode is only available with AES encryption
    private boolean AESOnly;

    public BlockMode(String _name, boolean _needsIV, boolean _needsPadding, boolean _needsCounter, boolean _AESOnly) {
        this.name = _name;
        this.needsIV = _needsIV;
        this.needsPadding = _needsPadding;
        this.needsCounter = _needsCounter;
        this.AESOnly = _AESOnly;
    }

    public boolean getNeedsIV() {
        return this.needsIV;
    }

    public String getName() {
        return this.name;
    }

    public boolean getNeedsPadding() {
        return this.needsPadding;
    }

    public boolean getNeedsCounter() {
        return this.needsCounter;
    }

    public boolean getAESOnly() {
        return this.AESOnly;
    }
}
