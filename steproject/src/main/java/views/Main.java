package views;

import controllers.MenuHandler;
import controllers.Plugin;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.geometry.Insets;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The main view of the secure text editor. This can be considered as the main frontend class. This class builds the view,
 * catches user events and directs them to the right controller and loads all plugins on startup as well as includes
 * the plugins frontend, e.g. the plugin menu.
 *
 * @author : Max Geldner
 * @version : 1.0i7
 */
public class Main extends Application {

    private TextArea textArea;
    private MenuBar menuBar;
    private LinkedHashMap<String, Object> pluginList;
    private Stage primaryStage;
    private String savedFile;
    private String title = "Secure Text Editor";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            this.savedFile = "";
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            // check the caller of the method to prevent plugins to execute the start method. If they could do it they could crash the ste on plugin initialization
            if(stackTraceElements[2].getClassName().equals("com.sun.javafx.application.LauncherImpl")) {
                primaryStage.setTitle(this.title);

                Group editorGroup = new Group();
                editorGroup.getChildren().addAll(this.renderMenu(), this.renderTextArea());

                Scene scene = new Scene(editorGroup);
                primaryStage.setScene(scene);
                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                primaryStage.getIcons().add(new Image("file:" + classloader.getResource("icons/steIcon.png").getPath()));
                primaryStage.show();

                this.primaryStage = primaryStage;

                // load all specified plugins and store them into a HashMap
                this.pluginList = Plugin.loadSTEPlugins(this);
                this.addPluginsToMenu();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Renders the top menu bar. Also adds all eventhandlers for the menu items.
     * @return MenuBar object with all added menus, menuitems and their properties
     */
    private MenuBar renderMenu() {
        this.menuBar = new MenuBar();
        this.menuBar.setPrefSize(900, 20);
        this.menuBar.setPadding(new Insets(0, 0, 0, 0));

        EventHandler<ActionEvent> action = handleActions();

        Menu menuFile = new Menu("File");
        MenuItem newFile = new MenuItem("New file");
        newFile.setOnAction(action);
        MenuItem openFile = new MenuItem("Open file");
        openFile.setOnAction(action);
        MenuItem saveFile = new MenuItem("Save file");
        saveFile.setOnAction(action);
        MenuItem exitProgram = new MenuItem("Exit");
        exitProgram.setOnAction(action);

        menuFile.getItems().addAll(newFile, openFile, saveFile, exitProgram);

        //Menu menuHelp = new Menu("Help");
        //MenuItem mechanisms = new MenuItem("What can we do?");
        //menuHelp.getItems().addAll(mechanisms);

        this.menuBar.getMenus().addAll(menuFile/*, menuHelp*/);

        return this.menuBar;
    }

    /**
     * Adds a menu for every plugin that has the property hasMenu activated. Call method addSubMenus of the plugin class.
     * A plugin can add as many sub menus as it likes but can not modify the main menu itself.
     */
    private void addPluginsToMenu() {
        for(Map.Entry<String, Object>  entry : this.pluginList.entrySet()) {
            String pname = entry.getKey();
            Object pclass = entry.getValue();

            LinkedHashMap<String, String> pluginProps = ((Plugin) pclass).getPluginProperties(pname);

            if(pluginProps != null && pluginProps.containsKey("hasMenu") && pluginProps.get("hasMenu").equals("1")) {
                Menu pluginMenu = new Menu(pname);
                pluginMenu = ((Plugin)pclass).addSubMenus(pluginMenu);
                this.menuBar.getMenus().add(pluginMenu);
            }
        }
    }

    /**
     * Renders the text area the user can write in.
     * @return TextArea object that has properties set in this method
     */
    private TextArea renderTextArea() {
        this.textArea = new TextArea();
        this.textArea.setPrefSize(900, 450);
        this.textArea.setLayoutY(25);
        this.textArea.textProperty().addListener((obs, oldValue, newValue)->{
            if((newValue.length() - oldValue.length()) == 1) {
                setTitleUnsaved();
            }
        });

        return textArea;
    }

    /**
     * Sets the title of the STE to an unsaved state (with an *)
     */
    public void setTitleUnsaved() {
        if(!this.primaryStage.getTitle().substring(this.primaryStage.getTitle().length() - 1).equalsIgnoreCase("*")) {
            this.primaryStage.setTitle(this.primaryStage.getTitle() + " *");
        }
    }

    /**
     * Sets the title of the STE to a saved state (without the *)
     */
    public void setTitleSaved() {
        this.primaryStage.setTitle(this.title);
    }

    /**
     * Getter for the content of the stages text area.
     * @return String of the user written text
     */
    public String getTextAreaContent() {
        return this.textArea.getText();
    }

    /**
     * Sets the content of the STE TextArea element.
     * @param text the String to set the content to
     */
    public void setTextAreaContent(String text) {
        this.setTitleSaved();
        this.textArea.setText(text);
    }

    /**
     * Renders a warn dialogue triggered from a plugin
     * @param pluginName the name of the plugin, which has to be in the active plugin list
     * @param alertTitle the title of the warning, should be a brief description or caption of the issue
     * @param alertMessage the actual text of the warning, should be a longer description and provide the use with a possible solution
     */
    public void showWarnDialogue(String pluginName, String alertTitle, String alertMessage) {
        MenuHandler menuHandler = new MenuHandler();
        if(menuHandler.isPNameValid(this.pluginList, pluginName)) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning from plugin " + pluginName);
            alert.setHeaderText(alertTitle);
            alert.setContentText(alertMessage);
            alert.showAndWait();
        } else {
            System.err.println("The Plugin the error message was sent from is not valid or not in the Plugin list!");
        }
    }

    // ******************
    // Event Handler
    // ******************

    /**
     * Catches every click event for the menubar and sends them to the controller. This methods has exception handling if an exception should
     * trigger a frontend dialogue. Else exceptions will be handled in the controller.
     * @return EvenHandler that handles the event for the clicked element
     */
    private EventHandler<ActionEvent> handleActions() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                MenuItem clicked = (MenuItem) event.getSource();
                String item = clicked.getText();
                try {
                    MenuHandler menuHandler = new MenuHandler();
                    if(item.equalsIgnoreCase("exit")) {
                        menuHandler.doExit();
                    } else if(item.equalsIgnoreCase("new file")) {
                        savedFile = "";
                        setTitleSaved();
                        textArea.setText(menuHandler.doNewFile());
                    } else if(item.equalsIgnoreCase("open file")) {
                        savedFile = "";
                        textArea.setText(menuHandler.doOpenFile(false, "")[1]);
                    } else if(item.equalsIgnoreCase("save file")) {
                        setTitleSaved();
                        if(savedFile.equals("")) {
                            savedFile = menuHandler.doSaveFile(textArea.getText().split("\\r?\\n"), false, "");
                        } else {
                            menuHandler.doSaveFile(textArea.getText().split("\\r?\\n"), true, savedFile);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
} // End of class Main