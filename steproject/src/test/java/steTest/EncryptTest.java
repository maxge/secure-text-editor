package steTest;

import controllers.MenuHandler;
import org.junit.Test;
import plugins.encrypt.controllers.Encrypt;
import plugins.encrypt.controllers.PBEncrypt;
import plugins.encrypt.models.Algorithm;
import plugins.encrypt.models.BlockMode;
import plugins.encrypt.models.ContentReader;
import views.Main;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EncryptTest {

    private String sampleText = "Test das Encrypt Plugins und der Encrypt Methode fuer DES und ECB!";

    /**
     * Tests encryption of DES without IV, AES with IV und ARC4.
     */
    @Test
    public void testSymmetricEncrypt() {
        Encrypt encrypt = new Encrypt();
        encrypt.loadPlugin(new Main());
        String[] result = encrypt.encryptSymmetric("DES", "ECB", "PKCS7Padding", 8, sampleText, "SHA-1", true);
        String[] result2 = encrypt.encryptSymmetric("AES", "CTR", "NoPadding", 24, sampleText, "SHA-1", true);
        String[] result3 = encrypt.encryptSymmetric("ARC4", "", "", 16, sampleText, "SHA-1", true);
        assertNotEquals(sampleText, result[1]);
        assertNotEquals(sampleText, result2[1]);
        assertNotEquals(sampleText, result3[1]);
        System.out.println("TESTED BASIC COMBINATIONS FOR ENCRYPTION FROM FILE");
    }

    /**
     * Tests decryption of same cases as the encryption test.
     */
    @Test
    public void testDecryptSymmetric() {
        MenuHandler mh = new MenuHandler();
        String[] result = mh.doOpenFile(true, "src\\main\\resources\\testDecrypt.secure");
        String[] result2 = mh.doOpenFile(true, "src\\main\\resources\\testDecryptNoPadding.secure");
        String[] result3 = mh.doOpenFile(true, "src\\main\\resources\\testDecryptStream.secure");
        Encrypt encrypt = new Encrypt();
        encrypt.loadPlugin(new Main());
        String realPlain = encrypt.decryptSymmetric(result, true);
        String realPlain2 = encrypt.decryptSymmetric(result2, true);
        String realPlain3 = encrypt.decryptSymmetric(result3, true);
        assertEquals(sampleText.trim().replace("\r",""), realPlain.trim().replace("\r",""));
        assertEquals(sampleText.trim().replace("\r",""), realPlain2.trim().replace("\r",""));
        assertEquals(sampleText.trim().replace("\r",""), realPlain3.trim().replace("\r",""));
        System.out.println("TESTED BASIC COMBINATIONS FOR DECRYPTION");
    }

    /**
     * Tests all combinations of non-PB encryption.
     */
    @Test
    public void testAllCombinations() {
        int counter = 0;
        Encrypt enc = new Encrypt();
        enc.loadPlugin(new Main());
        ContentReader cr = new ContentReader();
        LinkedHashMap<String, ArrayList> allAlgos = cr.getContent();
        for(int i = 0; i < allAlgos.get("Algorithms").size(); i++) {
            for(int j = 0; j < allAlgos.get("BlockModes").size(); j++) {
                for(int k = 0; k < allAlgos.get("Paddings").size(); k++) {
                    if(!allAlgos.get("Paddings").get(k).equals("NoPadding")) {
                        for (int l = 0; l < allAlgos.get("Hashings").size(); l++) {
                            String blockmode = ((BlockMode)allAlgos.get("BlockModes").get(j)).getName();
                            String padding = (String)allAlgos.get("Paddings").get(k);
                            int keylength = 16;
                            if (((Algorithm) allAlgos.get("Algorithms").get(i)).getName().equalsIgnoreCase("DES")) {
                                keylength = 8;
                            }
                            if(((Algorithm)allAlgos.get("Algorithms").get(i)).getName().equalsIgnoreCase("ARC4")) {
                                padding = null;
                                blockmode = null;
                            }
                            if(!(((BlockMode)allAlgos.get("BlockModes").get(j)).getAESOnly() && ((Algorithm)allAlgos.get("Algorithms").get(i)).getName().equalsIgnoreCase("DES"))) {
                                //System.out.println("NOW TESTING: " + ((Algorithm) allAlgos.get("Algorithms").get(i)).getName() + "/" + blockmode + "/" + padding + "/" + keylength + "/" + allAlgos.get("Hashings").get(l));
                                String[] result = enc.encryptSymmetric(((Algorithm) allAlgos.get("Algorithms").get(i)).getName(), blockmode, padding, keylength, this.sampleText, (String) allAlgos.get("Hashings").get(l), true);
                                assertNotEquals("", result[0]);
                                assertNotEquals("", result[1]);
                                assertNotEquals(this.sampleText, result[1]);
                                counter++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("TESTED " + counter + " NON-PBE COMBINATIONS FOR ENCRYPTION");
    }

    /**
     * Tests all combinations of PB Encryption.
     */
    @Test
    public void testPbeAllCombinations() {
        int counter = 0;
        Encrypt enc = new Encrypt();
        enc.loadPlugin(new Main());
        PBEncrypt pbenc = new PBEncrypt(enc);
        ContentReader cr = new ContentReader();
        LinkedHashMap<String, ArrayList> allAlgos = cr.getContent();
        for(int i = 0; i < allAlgos.get("Algorithms").size(); i++) {
            for (int l = 0; l < allAlgos.get("Hashings").size(); l++) {
                int keylength = 16;
                if (((Algorithm) allAlgos.get("Algorithms").get(i)).getName().equalsIgnoreCase("DES")) {
                    keylength = 8;
                }
                //System.out.println("NOW TESTING: " + ((Algorithm) allAlgos.get("Algorithms").get(i)).getName() + "/" + blockmode + "/" + padding + "/" + keylength + "/" + allAlgos.get("Hashings").get(l));
                String[] result = pbenc.encryptSymmetric(((Algorithm) allAlgos.get("Algorithms").get(i)).getName(), keylength, "password", this.sampleText, (String) allAlgos.get("Hashings").get(l), true);
                assertNotEquals("", result[0]);
                assertNotEquals("", result[1]);
                assertNotEquals(this.sampleText, result[1]);
                counter++;
            }
        }
        System.out.println("TESTED " + counter + " PBE COMBINATIONS FOR ENCRYPTION");
    }

    /**
     * Tests the basic functionality of pbe decryption.
     */
    @Test
    public void testPbeDecrypt() {
        Encrypt enc = new Encrypt();
        enc.loadPlugin(new Main());
        PBEncrypt pbenc = new PBEncrypt(enc);

        String[] encrypted = pbenc.encryptSymmetric("AES", 16, "1234", this.sampleText, "SHA-256", true);
        String[] openedFile = {"", encrypted[0] + encrypted[1]};
        String decrypted = pbenc.decryptSymmetric(openedFile, true, "1234");
        String decryptedWrongPW = pbenc.decryptSymmetric(openedFile, true, "7895");
        assertEquals(sampleText.trim().replace("\r",""), decrypted.trim().replace("\r",""));
        assertNotEquals(sampleText.trim().replace("\r",""), decryptedWrongPW.trim().replace("\r",""));
        System.out.println("TESTED BASIC PBE DECRYPTION");
    }
}
