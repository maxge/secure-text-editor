package steTest;

import controllers.Plugin;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import views.Main;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.LinkedHashMap;

import static org.junit.Assert.*;

public class PluginTest {

    /**
     * Tests the basic impelementation of the plugin reading process.
     */
    @Test
    public void loadSTEPluginsTest() {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            File fXmlFile = new File(classloader.getResource("settings.xml").getFile());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("plugin");

            if(nList.item(0) != null) {
                LinkedHashMap<String, Object> output = Plugin.loadSTEPlugins(new Main());
                assertNotEquals(true, output.isEmpty());
                System.out.println("TESTED LOADING OF ALL ACTIVE PLUGINS");
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
