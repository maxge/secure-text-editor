package steTest;

import controllers.MenuHandler;
import org.junit.Test;

import static org.junit.Assert.*;

public class MenuHandlerTest {

    MenuHandler mh;

    public MenuHandlerTest() {
        try {
            this.mh = new MenuHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the creation of a new file.
     */
    @Test
    public void testNewFile() {
        String output = this.mh.doNewFile();
        assertEquals("", output);
        System.out.println("TESTED CREATING OF NEW FILE");
    }

    /**
     * Tests if a file can be opened.
     */
    @Test
    public void testOpenFile() {
        String[] output = this.mh.doOpenFile(true, "src\\main\\resources\\testOpenFile.secure");
        assertEquals("Test ist erfolgreich!", output[1]);
        System.out.println("TESTED OPEN FILE");
    }

    /**
     * Tests if a file can be saved.
     */
    @Test
    public void testSaveFile() {
        String[] lines = new String[2];
        lines[0] = "Das ist die erste Zeile.";
        lines[1] = "Und die zweite Zeile kommt auch gleich hinterher!";
        String savePath = "src\\main\\resources\\testSaveFile.secure";
        try {
            this.mh.doSaveFile(lines, true,savePath);
            String[] output = this.mh.doOpenFile(true,savePath);
            assertEquals(lines[0] + lines[1], output[1]);
            System.out.println("TESTED SAVING FILE");
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Tests if excpetions get thrown when the application exit procedure is called.
     */
    @Test
    public void testExitApplication() {
        try {
            this.mh.doExit();
            System.out.println("TESTED APPLICATION EXIT PROCEDURE");
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
